import { Dispatch } from 'redux';
import * as appApi from '../../api/app';
import { ActionType } from '../action-types';
import { Action } from '../actions';

export const setModal = (modal: string): Action => ({
  type: ActionType.SET_MODAL,
  payload: modal
});

export const fetchCarouselData = () => {
  return async (dispatch: Dispatch<Action>) => {
    const res = await appApi.getCarouselData();
    dispatch({
      type: ActionType.FETCH_CAROUSEL_DATA,
      payload: res ? res.data : []
    });
  }
}