import { ActionType } from '../action-types';
import { Action } from '../actions';

export const setLogined = (status: boolean): Action => ({
  type: ActionType.SET_LOGINED,
  payload: status
});