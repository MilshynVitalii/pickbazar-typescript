import { ActionType } from '../action-types';
import { SlideCardData } from '../../components/Carousel/SlideCard';

interface SetModal {
  type: ActionType.SET_MODAL;
  payload: string;
}

interface SetLogined {
  type: ActionType.SET_LOGINED;
  payload: boolean;
}

interface FetchCarouselData {
  type: ActionType.FETCH_CAROUSEL_DATA;
  payload: SlideCardData[];
}

export type Action = SetModal | SetLogined | FetchCarouselData;