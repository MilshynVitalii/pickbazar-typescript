import { combineReducers } from 'redux';
import { appReducer } from './app';
import { authReducer } from './auth';

const reducers = combineReducers({
  app: appReducer,
  auth: authReducer
});

export default reducers;

export type RootState = ReturnType<typeof reducers>