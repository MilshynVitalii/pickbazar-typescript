import { Action } from '../actions';
import { ActionType } from '../action-types';

interface AuthState {
  logined: boolean;
}

const initialState = {
  logined: false
}

export const authReducer = (state: AuthState = initialState, action: Action): AuthState => {
  switch(action.type) {
    case ActionType.SET_LOGINED:
      return {...state, logined: action.payload};
    default:
      return state;
  }
};