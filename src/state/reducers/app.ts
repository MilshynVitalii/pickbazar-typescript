import { Action } from '../actions';
import { ActionType } from '../action-types';
import { SlideCardData } from '../../components/Carousel/SlideCard';

interface AppState {
  modal: string;
  carouselData: SlideCardData[];
}

const initialState = {
  modal: '',
  carouselData: []
}

export const appReducer = (state: AppState = initialState, action: Action): AppState => {
  switch(action.type) {
    case ActionType.SET_MODAL:
      return {...state, modal: action.payload};
    case ActionType.FETCH_CAROUSEL_DATA:
        return {...state, carouselData: [...action.payload]}
    default:
      return state;
  }
};