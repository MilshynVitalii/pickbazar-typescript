import {makeStyles} from '@material-ui/core/styles';
import {lightGreen} from '@material-ui/core/colors';

export default makeStyles({
  slideBtn: {
    position: 'absolute',
    top: '50%',
    transform: 'translateY(-50%)',
    backgroundColor: '#ffffff',
    boxShadow: '0 3px 6px 0 rgba(0,0,0, 0.16)',
    zIndex: 100,
    '&:hover': {
      backgroundColor: lightGreen[50],
    }
  },
  prevBtn: {
    left: '25px',
  },
  nextBtn: {
    right: '25px',
  }
});