import React from 'react';

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';

import useStyles from './slideStyles';

export interface Colors {
  start: string;
  end: string;
}

export interface SlideCardData {
  id: number;
  title: string;
  description: string;
  buttonText: string;
  gradientColors: Colors;
}

interface SlideCardProps {
  data: SlideCardData;
}

const SlideCard: React.FC<SlideCardProps> = ({data}: SlideCardProps): JSX.Element => {
  const styles = useStyles(data.gradientColors);

  return (
    <Card className={styles.slideCard} elevation={0}>
      <CardContent className={styles.content}>
        <Typography variant="h4" component="h3" className={styles.title}>{data.title}</Typography>
        <Typography variant="subtitle1" component="span" className={styles.subtitle}>{data.description}</Typography>
      </CardContent>
      <CardActions className={styles.action}>
        <Button size="small" color="primary" className={styles.slideCardBtn}>{data.buttonText}</Button>
      </CardActions>
    </Card>
  )
}

export default SlideCard;