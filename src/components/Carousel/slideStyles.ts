import {makeStyles} from '@material-ui/core/styles';
import { Colors } from './SlideCard';

export default makeStyles({
  title: {
    fontSize: '41px',
    marginBottom: '10px'
  },
  subtitle: {
    fontSize: '20px'
  },
  content: {
    padding: '90px 16px 0',
    textAlign: 'center'
  },
  action: {
    padding: '35px 16px 90px',
    justifyContent: 'center'
  },
  slideCard: (colors: Colors) => ({
    color: '#ffffff',
    background: `linear-gradient(90deg, ${colors.start}, ${colors.end})`
  }),
  slideCardBtn: (colors: Colors) => ({
    fontSize: '18px',
    color: colors.start,
    backgroundColor: '#ffffff',
    borderRadius: '180px',
    padding: '10px',
    width: '180px',
    '&:hover': {
      backgroundColor: '#ffffff',
      opacity: 0.8
    }
  })
});