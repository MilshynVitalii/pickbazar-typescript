import { AxiosResponse } from 'axios';
import api from './';
import { SlideCardData } from '../components/Carousel/SlideCard';

const getCarouselData = async (): Promise<AxiosResponse<SlideCardData[]>> => await api.get('/coupons');

export { getCarouselData };